import logo from './logo.svg';
import './App.css';
import TelephoneDirectory from './TelephoneDirectory';

function App() {
  return (
    <div className="App">
      <header className="App-header">     
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <input></input>
        <button>Save</button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
